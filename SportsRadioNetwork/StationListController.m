//
//  StationListController.m
//  SportsRadioNetwork
//
//  Created by Tim Hibbard on 12/14/12.
//  Copyright (c) 2012 Tim Hibbard. All rights reserved.
//

#import "StationListController.h"

@interface StationListController ()

@end

@implementation StationListController{
    CLLocationManager *locationManager;
    NSInteger rangeDelta;
}

@synthesize stationArray;
@synthesize pullTableView;

#pragma mark - Load and parse data

- (void) fetchByLocation{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    locationManager = [[CLLocationManager alloc]init];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    locationManager.delegate = (id)self;
    [locationManager startUpdatingLocation];
}

-(void) locationManager: (CLLocationManager *) manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    
    double GPSlat, GPSlng;
    GPSlat = locationManager.location.coordinate.latitude;
    GPSlng = locationManager.location.coordinate.longitude;
    if (GPSlat == 0) {
        return;
    }
    
    [locationManager stopUpdatingLocation];
    locationManager.delegate = nil;
    
    StationManager *stationManager = [[StationManager alloc] init];
    
    [stationManager getData:GPSlat :GPSlng :rangeDelta :^(NSString *errorMessage) {
        NSLog(@"%@",errorMessage);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        UIAlertView *errorView = [[UIAlertView alloc] initWithTitle:@"Error" message:errorMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorView show];
    } :^(NSMutableArray *jsonStationArray) {
        NSMutableArray *bindableArray = [[NSMutableArray alloc]init];
        for (int a = 0; a < [jsonStationArray count]; a++) {
            Station *newStation = (Station *)[jsonStationArray objectAtIndex:a];
            CLLocationDegrees lat = [newStation.lat doubleValue];
            CLLocationDegrees lng = [newStation.lng doubleValue];
            CLLocation *radioLocation = [[CLLocation alloc] initWithLatitude:lat longitude:lng];
            CLLocationDistance distance = [newLocation distanceFromLocation:radioLocation] * 0.000621371192;
            [newStation setSignalStrength:[[NSDecimalNumber alloc] initWithDouble:((newStation.range.doubleValue - distance)/newStation.range.doubleValue)] ];
            //[newStation setDistance:[NSString stringWithFormat:@"%d miles", (int) ceil(distance)]];
            [newStation setDistance: [NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithInt:(int) ceil(distance)]decimalValue]]];
            if (distance <= [newStation.range doubleValue] + rangeDelta) {
                [bindableArray addObject:newStation];
            }
            
            
            
        }
        [self setStationArray:bindableArray];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        [stationArray sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            Station *st1 = (Station *)obj1;
            Station *st2 = (Station *)obj2;
            
            return [st2.signalStrength compare:st1.signalStrength];
            
        }];
        
        [self performSelectorOnMainThread:@selector(refreshTableView) withObject:nil waitUntilDone:YES];
        
    }];
    
    
    
    
    self.pullTableView.pullLastRefreshDate = [NSDate date];
    self.pullTableView.pullTableIsRefreshing = NO;
    self.pullTableView.pullTableIsLoadingMore = NO;
    
    
}

- (void) locationManager: (CLLocationManager *) manager didFailWithError:(NSError *)error {
    [locationManager stopUpdatingLocation];
    locationManager.delegate = nil;
    self.pullTableView.pullLastRefreshDate = [NSDate date];
    self.pullTableView.pullTableIsRefreshing = NO;
    self.pullTableView.pullTableIsLoadingMore = NO;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    NSLog(@"locationManager didFailWithError:%@",error);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"GPS Error" message:@"Could not get GPS info. Please enable if disabled then pull to refresh." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    //throw up a message
}

- (void) refreshTableView{
    [self.tableView reloadData];

    //uncomment after adding searchbar
    //self.tableView.contentOffset = CGPointMake(0, self.searchBar.frame.size.height);
}

#pragma mark - PullTableViewDelegate

- (void)pullTableViewDidTriggerRefresh:(PullTableView *)pullTableView{
    
    [self performSelector:@selector(refreshTable) withObject:nil afterDelay:0.1f];
}

- (void)pullTableViewDidTriggerLoadMore:(PullTableView *)pullTableView{
    [self performSelector:@selector(loadMoreDataToTable) withObject:nil afterDelay:0.5f];
}

#pragma mark - refreshing

- (void) refreshTable{
    /*
     
     Code to actually refresh goes here.
     
     */
    
    rangeDelta = 0;
    
    [self fetchByLocation];
    
}

- (void) loadMoreDataToTable{
    /*
     
     Code to actually load more data goes here.
     
     */
    if (!rangeDelta) {
        rangeDelta = 0;
    }
    
    rangeDelta = rangeDelta + 5;
    [self fetchByLocation];
    
    
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    if(!self.pullTableView.pullTableIsRefreshing) {
        self.pullTableView.pullTableIsRefreshing = YES;
        [self performSelector:@selector(refreshTable) withObject:nil afterDelay:1];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(becomeActive:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    

    
//    UISwipeGestureRecognizer *rightSwipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight:)];
//    [rightSwipeGesture setDirection:UISwipeGestureRecognizerDirectionRight];
//    UISwipeGestureRecognizer *leftSwipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft:)];
//    [leftSwipeGesture setDirection:UISwipeGestureRecognizerDirectionLeft];
//    
//    [self.pullTableView addGestureRecognizer:rightSwipeGesture];
//    [self.pullTableView addGestureRecognizer:leftSwipeGesture];
//    
//    [self setHelpText:@"Swipe left to see more info"];
//    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [self setHelpText:[defaults objectForKey:@"helpText"]];
//    if (helpText.length == 0) {
//        [self setHelpText:@"Swipe left to see more info"];
//    }
//    
//    [defaults setObject:helpText forKey:@"helpText"];
//    [defaults synchronize];
}

-(void)becomeActive:(NSNotification *)notification {
    // only respond if the selected tab is our current tab
    if ([self.navigationController.visibleViewController isKindOfClass:[StationListController class]]) {
        NSLog(@"view controller = %@",self.navigationController.visibleViewController);
        
        
        NSTimeInterval diff = [[NSDate date] timeIntervalSinceDate:self.pullTableView.pullLastRefreshDate];
        NSLog(@"Diff = %lf",diff);
        if(!self.pullTableView.pullTableIsRefreshing && ((diff / 60) > 5)) {
            //self.pullTableView.pullTableIsRefreshing = YES;
            [self performSelector:@selector(refreshTable) withObject:nil afterDelay:1];
        }
    }
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    int sections = 0;
    //find number of stations in range
    NSIndexSet *inRange = [stationArray indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        return [((Station*) obj).signalStrength floatValue] > 0;
    }];
    
    if (inRange.count == 1)
        sections = 1;
    else if (inRange.count > 1)
        sections = 2;
    
    NSLog(@"inrange:%@",[NSNumber numberWithInt:inRange.count]);
    
    //find number of stations out of range
    NSIndexSet *outOfRange = [stationArray indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        return [((Station*) obj).signalStrength floatValue] <= 0;
    }];
    if (outOfRange.count > 0) {
        sections = sections + 1;
    }

    return sections;
    
}


- (NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 0:{
            NSIndexSet *inRange = [stationArray indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                return [((Station*) obj).signalStrength floatValue] > 0;
            }];
            if (inRange.count > 0) {
                return @"Best Station";
            }
            
            //no stations in range...we can assume all stations are out of range
            NSIndexSet *outOfRange = [stationArray indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                return [((Station*) obj).signalStrength floatValue] <= 0;
            }];
            if (outOfRange.count > 1) {
                return @"Stations not in range";
            }
            return @"Station not in range";
            break;
        }
        case 1:{
            NSIndexSet *inRange = [stationArray indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                return [((Station*) obj).signalStrength floatValue] > 0;
            }];
            if (inRange.count > 2) {
                return @"Alternative stations";
            }
            else if (inRange.count == 2){
                return @"Alternative station";
            }
            
            NSIndexSet *outOfRange = [stationArray indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                return [((Station*) obj).signalStrength floatValue] <= 0;
            }];
            if (outOfRange.count > 1) {
                return @"Stations not in range";
            }
            return @"Station not in range";
            break;
            
            break;
        }
            
            
        case 2:{
            NSIndexSet *indexes = [stationArray indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                return [((Station*) obj).signalStrength floatValue] <= 0;
            }];
            
            if (indexes.count > 1) {
                return @"Stations not in range";
            }
            return @"Station not in range";
            break;
        }            
        default:
            return nil;
            break;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:{
            NSIndexSet *inRange = [stationArray indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                return [((Station*) obj).signalStrength floatValue] > 0;
            }];
            if (inRange.count > 0) {
                return 1;
            }
            
            //no stations in range...we can assume all stations are out of range
            NSIndexSet *outOfRange = [stationArray indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                return [((Station*) obj).signalStrength floatValue] <= 0;
            }];
            
            return outOfRange.count;
            break;
        }
        case 1:{
            NSIndexSet *inRange = [stationArray indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                return [((Station*) obj).signalStrength floatValue] > 0;
            }];
            if (inRange.count > 1) {
                return inRange.count - 1;
            }
            
            NSIndexSet *outOfRange = [stationArray indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                return [((Station*) obj).signalStrength floatValue] <= 0;
            }];
            
            return outOfRange.count;
            break;
        }
            
            
        case 2:{
            NSIndexSet *indexes = [stationArray indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                return [((Station*) obj).signalStrength floatValue] <= 0;
            }];
            return indexes.count;
            break;
        }
        default:
            return 0;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    NSInteger row = 0;
    
    switch (indexPath.section) {
        case 0:
            row = indexPath.row;
            break;
        case 1:
            row = [tableView numberOfRowsInSection:0] + indexPath.row;
            break;
        case 2:
            row = [tableView numberOfRowsInSection:0] + [tableView numberOfRowsInSection:1] + indexPath.row;
            break;
        default:
            break;
    }
    
    Station* station = [stationArray objectAtIndex:row];
    
    [cell.textLabel setText:[NSString stringWithFormat:@"%@ %@ %@",station.frequency,station.frequencyType, station.stationName]];
    [cell.detailTextLabel setText:[NSString stringWithFormat:@"%@, %@",station.city,station.state]];
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
