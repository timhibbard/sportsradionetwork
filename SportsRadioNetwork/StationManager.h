//
//  StationManager.h
//  SportsRadioNetwork
//
//  Created by Tim Hibbard on 12/14/12.
//  Copyright (c) 2012 Tim Hibbard. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Station.h"
#import "srnAppDelegate.h"


@interface StationManager : NSObject{
    NSMutableData *webData;
    NSURLConnection *conn;
}

- (void) getData: (double ) gpsLat : (double ) gpsLng : (NSInteger ) range : (void (^) (NSString *errorMessage)) executeOnFailure : (void (^)(NSMutableArray *jsonStationArray)) executeOnSuccess;

- (void) getData: (NSString *) stationName : (void (^) (NSString *errorMessage)) executeOnFailure : (void (^)(NSMutableArray *jsonStationArray)) executeOnSuccess;

@end
