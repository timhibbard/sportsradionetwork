//
//  Station.h
//  SportsRadioNetwork
//
//  Created by Tim Hibbard on 12/16/12.
//  Copyright (c) 2012 Tim Hibbard. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Station : NSManagedObject

@property (nonatomic, retain) NSString * backgroundStreamUrl;
@property (nonatomic, retain) NSString * callSign;
@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSNumber * databaseId;
@property (nonatomic, retain) NSDecimalNumber * distance;
@property (nonatomic, retain) NSString * facebook;
@property (nonatomic, retain) NSString * frequency;
@property (nonatomic, retain) NSString * frequencyType;
@property (nonatomic, retain) NSDecimalNumber * lat;
@property (nonatomic, retain) NSDecimalNumber * lng;
@property (nonatomic, retain) NSDecimalNumber * range;
@property (nonatomic, retain) NSDecimalNumber * signalStrength;
@property (nonatomic, retain) NSString * state;
@property (nonatomic, retain) NSString * stationName;
@property (nonatomic, retain) NSString * streamUrl;
@property (nonatomic, retain) NSString * twitter;
@property (nonatomic, retain) NSString * website;
@property (nonatomic, retain) NSString * team;

@end
