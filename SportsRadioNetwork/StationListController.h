//
//  StationListController.h
//  SportsRadioNetwork
//
//  Created by Tim Hibbard on 12/14/12.
//  Copyright (c) 2012 Tim Hibbard. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullTableView.h"
#import <CoreLocation/CoreLocation.h>
#import "StationManager.h"
#import "Station.h"

@interface StationListController : UITableViewController<UITableViewDataSource, PullTableViewDelegate, UIGestureRecognizerDelegate, UISearchBarDelegate>{
    
}

@property (nonatomic) NSMutableArray *stationArray;
@property (nonatomic, retain) IBOutlet PullTableView *pullTableView;

@end
