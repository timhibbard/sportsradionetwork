//
//  srnAppDelegate.h
//  SportsRadioNetwork
//
//  Created by Tim Hibbard on 12/14/12.
//  Copyright (c) 2012 Tim Hibbard. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface srnAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end
