//
//  StationManager.m
//  SportsRadioNetwork
//
//  Created by Tim Hibbard on 12/14/12.
//  Copyright (c) 2012 Tim Hibbard. All rights reserved.
//

#import "StationManager.h"

static void (^failure)(NSString *) = nil;
static void (^success)(NSMutableArray *) = nil;

@implementation StationManager

#define NULL_TO_NIL(obj) ({ __typeof__ (obj) __obj = (obj); __obj == [NSNull null] ? nil : obj; })

- (void) getData:(NSString *)stationName :(void (^)(NSString *))executeOnFailure :(void (^)(NSMutableArray *))executeOnSuccess{
    failure = executeOnFailure;
    success = executeOnSuccess;

    NSString *sq = [NSString stringWithFormat:@"ownedby=\"%@\" or callsign=\"%@\" or callsign=\" %@ \"",[stationName stringByReplacingOccurrencesOfString:@" " withString:@"+"],stationName, stationName];
    NSString * encodedString = (__bridge_transfer NSString *)CFURLCreateStringByAddingPercentEscapes(
                                                                                                     NULL,
                                                                                                     (__bridge CFStringRef)sq,
                                                                                                     NULL,
                                                                                                     (CFStringRef)@"!*'();:@&=$,/?%#[]",
                                                                                                     kCFStringEncodingUTF8 );
    NSString *urlString = [NSString stringWithFormat:@"https://spreadsheets.google.com/feeds/list/0Ahgt-_OuYTzMdHhLMEx2eW1pTmEyYV95S3pCSndnNEE/od6/public/values?alt=json&sq=%@",encodedString];
    NSLog(@"url=%@",urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    conn = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
    if (conn) {
        webData = [NSMutableData data];
    }
    else {
        NSLog(@"connection failed");
    }
    
}

- (void) getData:(double)gpsLat :(double)gpsLng : (NSInteger ) range :(void (^)(NSString *))executeOnFailure :(void (^)(NSMutableArray *))executeOnSuccess{
    
    failure = executeOnFailure;
    success = executeOnSuccess;
    double delta = 2.2;
    if (range > 0) {
        delta = delta + ((int)range / (double)60);
    }
    NSLog(@"delta:%lf",delta);
    NSString *sq = [NSString stringWithFormat:@"lat < %lf and lat > %lf and lng > %lf and lng < %lf",gpsLat + delta, gpsLat - delta, gpsLng - delta, gpsLng + delta];
    
    NSString * encodedString = (__bridge_transfer NSString *)CFURLCreateStringByAddingPercentEscapes(
                                                                                                     NULL,
                                                                                                     (__bridge CFStringRef)sq,
                                                                                                     NULL,
                                                                                                     (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                                     kCFStringEncodingUTF8 );
    
    NSString *urlString = [NSString stringWithFormat:@"https://spreadsheets.google.com/feeds/list/0Ahgt-_OuYTzMdHhLMEx2eW1pTmEyYV95S3pCSndnNEE/od6/public/values?alt=json&sq=%@",encodedString];
    NSLog(@"url=%@",urlString);
    NSURL *url = [NSURL URLWithString:urlString];

    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    conn = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
    if (conn) {
        webData = [NSMutableData data];
    }
    else {
        NSLog(@"connection failed");
    }
}

- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    [webData setLength:0];
}

- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [webData appendData:data];
}

- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    failure(@"Connection error");
    NSLog(@"%@",[error localizedDescription]);
    
}

- (void) connectionDidFinishLoading:(NSURLConnection *)connection{
    NSLog(@"DONE. Received Bytes: %d", [webData length]);
    if (webData.length == 0) {
        failure(@"no data");
        return;
        
    }
    
    NSError *error;
    
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:webData options:kNilOptions error:&error];
    
    NSMutableArray *stations = [[NSMutableArray alloc] init];
    
    NSDictionary *entries = [[json objectForKey:@"feed"] objectForKey:@"entry"];
    
    for (NSDictionary *array in entries) {

        NSManagedObjectContext *moc = [(srnAppDelegate*)[UIApplication sharedApplication].delegate managedObjectContext];
        NSEntityDescription *stationDescription = [NSEntityDescription entityForName:@"Station" inManagedObjectContext:moc];
        Station *station = [[Station alloc] initWithEntity:stationDescription insertIntoManagedObjectContext:moc];
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        [f setNumberStyle:NSNumberFormatterDecimalStyle];
        
        [station setBackgroundStreamUrl:[self parse:array :@"backgroundstreaming"]];
        [station setCallSign:[self parse:array :@"callsign"]];
        [station setCity:[self parse:array :@"city"]];
        [station setDatabaseId:[f numberFromString:[self parse:array :@"id"]]];
        [station setFacebook:[self parse:array :@"facebook"]];
        [station setFrequency:[self parse:array :@"freq"]];
        [station setFrequencyType:[self parse:array :@"freqtype"]];
        [station setLat:[NSDecimalNumber decimalNumberWithString:[self parse:array :@"lat"]]];
        [station setLng:[NSDecimalNumber decimalNumberWithString:[self parse:array :@"lng"]]];
        [station setRange:[NSDecimalNumber decimalNumberWithString:[self parse:array :@"range"]]];
        [station setState:[self parse:array :@"state"]];
        [station setStationName:[self parse:array :@"stationname"]];
        [station setStreamUrl:[self parse:array :@"streamurl"]];
        [station setTeam:[self parse:array :@"team"]];
        [station setTwitter:[self parse:array :@"twitter"]];
        [station setWebsite:[self parse:array :@"website"]];
        
        [stations addObject:station];
    }
    
    
    NSLog(@"station count:%d",stations.count);
    
    success(stations);
}

- (NSString *) parse: (NSDictionary *)dict : (NSString*) key{
    if ([key rangeOfString:@"gsx$"].location == NSNotFound) {
        key = [NSString stringWithFormat:@"gsx$%@",key];
    }
    return NULL_TO_NIL([[dict objectForKey:key] objectForKey:@"$t"]);
}



@end
