//
//  Station.m
//  SportsRadioNetwork
//
//  Created by Tim Hibbard on 12/16/12.
//  Copyright (c) 2012 Tim Hibbard. All rights reserved.
//

#import "Station.h"


@implementation Station

@dynamic backgroundStreamUrl;
@dynamic callSign;
@dynamic city;
@dynamic databaseId;
@dynamic distance;
@dynamic facebook;
@dynamic frequency;
@dynamic frequencyType;
@dynamic lat;
@dynamic lng;
@dynamic range;
@dynamic signalStrength;
@dynamic state;
@dynamic stationName;
@dynamic streamUrl;
@dynamic twitter;
@dynamic website;
@dynamic team;

@end
